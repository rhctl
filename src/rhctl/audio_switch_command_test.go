//
//  rhctl
//
//  Copyright (C) 2009-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhctl.
//
//  rhctl is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhctl is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhctl. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"testing"
)

func TestSwitchCommandState(t *testing.T) {
	var result string
	var err error

	if result, err = SwitchCmdStateAudio.Generate(SwitchUnitID(2)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*2SL" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdStateSilence.Generate(SwitchUnitID(1)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*1SS" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdStateGPI.Generate(SwitchUnitID(0), SwitchGPINum(4)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*0SP04" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdStateGPIAll.Generate(SwitchUnitID(0)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*0SPA" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdStateRelay.Generate(SwitchUnitID(3)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*3SR" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdStateOC.Generate(SwitchUnitID(1)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*1SO" {
		t.Fatalf("wrong output: got '%s'", result)
	}
}

func TestSwitchCommandAudio(t *testing.T) {
	var result string
	var err error

	if result, err = SwitchCmdAudioApplyInput.Generate(SwitchUnitID(0), SwitchInputNum(8), SwitchOutputNum(1)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*0081" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioApplyInputAll.Generate(SwitchUnitID(0), SwitchInputNum(3)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*003A" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioAddInputTo1.Generate(SwitchUnitID(3), SwitchInputNum(4)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*3043" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioAddInputTo2.Generate(SwitchUnitID(0), SwitchInputNum(1)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*0014" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioRemoveInputFrom1.Generate(SwitchUnitID(2), SwitchInputNum(7)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*2075" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioRemoveInputFrom2.Generate(SwitchUnitID(1), SwitchInputNum(2)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*1026" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioMuteInput.Generate(SwitchUnitID(1), SwitchInputNum(2), SwitchOutputNum(2)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*102M2" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioMuteInputAll.Generate(SwitchUnitID(1), SwitchInputNum(2)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*102MA" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioMuteOutput.Generate(SwitchUnitID(0), SwitchOutputNum(1)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*0M1" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioMuteOutputAll.Generate(SwitchUnitID(3)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*3MA" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioFadeUpInput.Generate(SwitchUnitID(0), SwitchInputNum(3)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*0FU03" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdAudioFadeDownInput.Generate(SwitchUnitID(1), SwitchInputNum(7)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*1FD07" {
		t.Fatalf("wrong output: got '%s'", result)
	}
}

func TestSwitchCommandRelay(t *testing.T) {
	var result string
	var err error

	if result, err = SwitchCmdRelayUnlatch.Generate(SwitchUnitID(3), SwitchRelayNum(3)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*3OR3F" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdRelayLatch.Generate(SwitchUnitID(1), SwitchRelayNum(6)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*1OR6L" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdRelayPulse.Generate(SwitchUnitID(1), SwitchRelayNum(4)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*1OR4P" {
		t.Fatalf("wrong output: got '%s'", result)
	}
}

func TestSwitchCommandOC(t *testing.T) {
	var result string
	var err error

	if result, err = SwitchCmdOCUnlatch.Generate(SwitchUnitID(2), SwitchOCNum(3)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*2OO3F" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdOCLatch.Generate(SwitchUnitID(3), SwitchOCNum(2)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*3OO2L" {
		t.Fatalf("wrong output: got '%s'", result)
	}

	if result, err = SwitchCmdOCPulse.Generate(SwitchUnitID(0), SwitchOCNum(7)); err != nil {
		t.Fatal("unexpected error:", err)
	}
	if result != "*0OO7P" {
		t.Fatalf("wrong output: got '%s'", result)
	}
}
