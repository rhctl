//
//  rhctl
//
//  Copyright (C) 2009-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhctl.
//
//  rhctl is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhctl is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhctl. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var (
	rhl  = log.New(os.Stderr, "[rhctl]\t", log.LstdFlags)
	rhdl = log.New(ioutil.Discard, "[rhctl-dbg]\t", log.LstdFlags)
)

func init() {
	if _, exists := os.LookupEnv("RHCTL_DEBUG"); exists {
		rhdl.SetOutput(os.Stderr)
	}
}

type envStringValue string

func newEnvStringValue(key, dflt string) *envStringValue {
	if envval, exists := os.LookupEnv(key); exists {
		return (*envStringValue)(&envval)
	} else {
		return (*envStringValue)(&dflt)
	}
}

func (s *envStringValue) Set(val string) error {
	*s = envStringValue(val)
	return nil
}

func (s *envStringValue) Get() interface{} { return string(*s) }

func (s *envStringValue) String() string { return fmt.Sprintf("%s", *s) }

func main() {
	configFile := newEnvStringValue("RHCTL_CONFIG_FILE", "/etc/rhctl/config.toml")
	flag.Var(configFile, "conf", "path to the configuration file (environment: RHCTL_CONFIG_FILE)")
	help := flag.Bool("help", false, "show usage")

	flag.Parse()
	if *help {
		flag.Usage()
		return
	}

	conf, err := ReadConfig(configFile.Get().(string))
	if err != nil {
		rhl.Println("Error reading configuration:", err.Error())
		return
	}
	rhl.Printf("just started...")
	rhdl.Printf("configuration: %+v", conf)

	//**************************************************
	// initializing essential parts
	sw, err := SwitchInit(conf)
	if err != nil {
		rhl.Println("error initializing audio switch: ", err)
		return
	}

	var servers []*PlayoutServer
	for s, _ := range conf.Servers {
		server, err := ServerInit(s, conf)
		if err != nil {
			rhl.Printf("error initializing playout-server(%s): %v", s, err)
			return
		}
		servers = append(servers, server)
	}
	if len(servers) <= 0 {
		rhl.Printf("Error: there is no playout server configured...")
		return
	}

	ctrl := SwitchControlInit(conf, sw, servers)

	// initializing non-essential parts, like control-interfaces aka clients
	telnet, err := TelnetInit(conf, ctrl)
	if err != nil {
		rhl.Printf("Error: failed to initialize telnet interface: %v", err)
		return
	}
	web := WebInit(conf, ctrl)
	nop, err := NopTCPInit(conf, ctrl)
	if err != nil {
		rhl.Printf("Error: failed to initialize nop-tcp interface: %v", err)
		return
	}

	//**************************************************
	// running essential parts
	stop := make(chan bool)

	go func() {
		rhl.Printf("starting audioswitch handler")
		sw.Run()
		rhl.Printf("audioswitch handler has stopped")
		stop <- true
	}()

	for _, server := range servers {
		go func(server *PlayoutServer) {
			rhl.Printf("starting playout-server(%s) handler", server.name)
			server.Run()
			rhl.Printf("playout-server(%s) handler has stopped", server.name)
			stop <- true
		}(server)
	}

	go func() {
		rhl.Printf("starting switch control")
		ctrl.Run()
		rhl.Printf("switch control has stopped")
		stop <- true
	}()

	// running non-essential parts
	if conf.Clients.Telnet.Address != "" {
		go func() {
			rhl.Printf("starting telnet interface")
			telnet.Run()
			rhl.Printf("telnet interface just stopped")
		}()
	}

	if conf.Clients.Web.Address != "" {
		go func() {
			rhl.Printf("starting web interface")
			web.Run()
			rhl.Printf("web interface just stopped")
		}()
	}

	if conf.Clients.Noptcp.Address != "" {
		go func() {
			rhl.Printf("starting nop-tcp interface")
			nop.Run()
			rhl.Printf("nop-tcp interface just stopped")
		}()
	}

	//**************************************************
	<-stop
	rhl.Printf("at least one essential part has stopped - bringing down the whole process")
}
